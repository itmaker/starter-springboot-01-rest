package com.tsel.itmaker.starter.rest;

import javax.annotation.PostConstruct;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContextException;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ApplicationConfig  implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
      /* 
      final Environment environment = configurableListableBeanFactory.getBean(Environment.class);
  
        String activeProfileKey = "SPRING_PROFILES_ACTIVE";
        if(environment.getProperty(activeProfileKey) == null) {
           throw new ApplicationContextException("Missing property on context bootstrap: " + activeProfileKey);
        }*/
     }
}