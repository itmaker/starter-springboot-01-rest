package com.tsel.itmaker.starter.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class Application implements ApplicationRunner {
	@Autowired
	private Environment environment;

	private static final Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(final String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		// logger.info("Application name: {}", appName.getName());
		final String envProfile = environment.getProperty("SPRING.PROFILES.ACTIVE");
		if (envProfile == null) {
			logger.info("Environment Variable Not Specified: SPRING_PROFILES_ACTIVE");
			// System.exit(1);
		}
	}

}
