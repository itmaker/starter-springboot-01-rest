package com.tsel.itmaker.starter.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;

import org.springframework.context.event.EventListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEventListener {
    Logger logger = LoggerFactory.getLogger(ApplicationEventListener.class);
    
    @Autowired
    private Environment environment;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ConfigurableApplicationContext cfgCtx;
    //private static final Logger LOG 
     // = Logger.getLogger(EventListenerExampleBean.class);
 
    @EventListener
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        //LOG.info("Increment counter");
        String envProfile = environment.getProperty("SPRING.PROFILES.ACTIVE");
        if (envProfile == null) {
            logger.info("Environment Variable Not Specified: SPRING_PROFILES_ACTIVE");
            // System.exit(1);
        }
        // context.
    }
}